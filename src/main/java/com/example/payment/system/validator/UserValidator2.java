package com.example.payment.system.validator;

import com.example.payment.system.entity.User;
import com.example.payment.system.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.ast.Not;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
//@RequiredArgsConstructor
@Component
public class UserValidator2 implements Validator {
    private final UserRepository userRepository;

    public UserValidator2(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }
    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        if (userRepository.existsByEmail(user.getEmail())) {
            errors.rejectValue("email", "field.unique", "User with this email is already exist");
        }

        if (!(user.getTelephone().length() == 11 && user.getTelephone().charAt(0) == '8')) {
            errors.rejectValue("telephone","pattern.invalid", "telephone Number should start with '8' and consist with 12 numbers");
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            errors.rejectValue("telephone", "field.unique", "User with this phone number is already exist");
        }
    }
}
