package com.example.payment.system.service;

import com.example.payment.system.entity.Product;
import com.example.payment.system.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final EntityManager entityManager;
    public List<Product> findByFilters(String title, Double price, UUID user) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> root = criteriaQuery.from(Product.class);

        List<Predicate> predicates = new ArrayList<>();
        // Добавление условий фильтрации в список predicates в зависимости от переданных параметров
        if (price != null) {
            predicates.add(criteriaBuilder.equal(root.get("price"), price));
        }

        if (title != null) {
            predicates.add(criteriaBuilder.like(root.get("title"), "%" + title + "%"));
        }
        if (user != null) {
            predicates.add(criteriaBuilder.equal(root.get("user"), user));
        }

        // Комбинирование условий фильтрации с помощью criteriaBuilder.and()
        criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[0])));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
    public Product save(Product std) {
        return productRepository.save(std);
    }
}
