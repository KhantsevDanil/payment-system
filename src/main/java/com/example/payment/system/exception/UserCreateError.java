package com.example.payment.system.exception;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Map;
@RequiredArgsConstructor
@AllArgsConstructor
public class UserCreateError extends RuntimeException {
    private Map<String, String> errorMessages;

    public Map<String, String> getMap() {
        return errorMessages;
    }
}