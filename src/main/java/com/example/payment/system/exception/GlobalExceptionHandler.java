package com.example.payment.system.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(UserCreateError.class)
    public ResponseEntity<Object> handleCustomException(UserCreateError ex) {
        // Формируем JSON-ответ с данными из исключения
        Map<String, Object> response = new HashMap<>();
        response.put("success", false);
        response.put("message", "Validation failed");
        response.put("errors", ex.getMap());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    // Другие методы обработчиков исключений
}
