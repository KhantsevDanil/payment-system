package com.example.payment.system.auth;

import com.example.payment.system.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
  private String PhoneNumber;
  private String firstname;
  private String lastname;
  private String email;
  private String password;
  private Role role;
}
