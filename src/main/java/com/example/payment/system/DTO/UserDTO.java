package com.example.payment.system.DTO;

import com.example.payment.system.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;

@AllArgsConstructor
@Builder
@Data
public class UserDTO {
    String first_name;
    String last_name;
    @Email
    String email;
    String telephone;
    String password;
    Role role;
}
