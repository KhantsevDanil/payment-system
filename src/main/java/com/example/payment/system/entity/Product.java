package com.example.payment.system.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Data
@Entity
public class Product {
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id @Setter(AccessLevel.PROTECTED) UUID id;

    @Column(scale = 2, nullable = false)
    private Double price;

    @Column(length=20, unique = true, nullable = false)
    private String title;

    @Column(length=100)
    private String description;


    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL)
    private List<ProductPhoto> photo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "discount_id")
    private Discount discount;
}