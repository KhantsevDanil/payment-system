package com.example.payment.system.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Data
@Entity
public class Balance {
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id
    @Setter(AccessLevel.PROTECTED) UUID id;
    public enum Choice {
        dollar,
        ruble,
        euro
    }

    @Min(0)
    @Column(scale = 2)
    private Double sum;

    @Column
    @Enumerated(EnumType.STRING)
    private Choice currency;

    @OneToMany(mappedBy = "balance", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    private List<User> users;

    @OneToMany(mappedBy = "balance", cascade = CascadeType.ALL)
    private List<Transaction> transactions;
}
