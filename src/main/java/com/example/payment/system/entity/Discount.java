package com.example.payment.system.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Data
@Entity
public class Discount {

    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id
    @Setter(AccessLevel.PROTECTED) UUID id;

    @Column
    private Integer amount;

    @OneToMany(mappedBy = "discount", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    private List<Product> products;
}
