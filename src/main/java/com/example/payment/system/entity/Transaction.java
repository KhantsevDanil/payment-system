package com.example.payment.system.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@RequiredArgsConstructor
@Data
@Entity
public class Transaction {
    public enum Choice {
        adopted,
        in_processing,
        completed,
        cancelled
    }
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id
    @Setter(AccessLevel.PROTECTED) UUID id;

    @OneToOne
    private User recipient;

    @OneToOne
    private User sender;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Choice status = Transaction.Choice.valueOf("adopted");

    @Column(scale = 2, length = 200)
    private String description;

    @Column(scale = 2)
    private boolean value;

    @Column
    @Enumerated(EnumType.STRING)
    private Balance.Choice currency;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @JoinColumn(name = "balance_id")
    private Balance balance;
}
