package com.example.payment.system.entity;

import javax.persistence.*;

//import com.example.payment.system.validator.ValidUser;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_user")
//@ValidUser
public class User implements UserDetails {


  @GeneratedValue(generator = "uuid4")
  @GenericGenerator(name = "UUID", strategy = "uuid4")
  private @Id
  @Setter(AccessLevel.PROTECTED) UUID id;


  private String firstname;
  private String lastname;

//  @Email
  @NotBlank(message = "Field must not be blank")
  @Column(unique = true)
  private String email;

  private String password;

  @NotBlank(message = "Field must not be blank")
  @Column(name = "telephone_number", length = 20, unique = true)
  private String telephone;

//  @NotBlank(message = "Field must not be blank")
  @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
  @JoinColumn(name = "balance_id")
  private Balance balance;

  @Column(name = "preferred_currency")
  @Enumerated(EnumType.STRING)
  private Balance.Choice currency;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
  private List<Product> products;

//  public User(String telephone, String email) {
//    this.telephone = telephone;
//    this.email = email;
//
//    ValidateUser validator = new ValidateUser();
//    validator.validatePhoneNumber(telephone);
//    validator.validateUserName(email);
//  }

  @Enumerated(EnumType.STRING)
  private Role role;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
  private List<Token> tokens;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return role.getAuthorities();
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
