package com.example.payment.system.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

import javax.persistence.*;
import java.sql.Blob;

@RequiredArgsConstructor
@Data
@Entity
public class ProductPhoto {
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "UUID", strategy = "uuid4")
    private @Id
    @Setter(AccessLevel.PROTECTED) UUID id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Product product;

    @Lob
    @Column
    private Blob image;
}