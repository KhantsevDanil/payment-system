package com.example.payment.system.repository;

import com.example.payment.system.entity.Balance;
import com.example.payment.system.entity.Discount;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, UUID> {

}