package com.example.payment.system.repository;

import com.example.payment.system.entity.User;
//import com.example.payment.system.validator.ValidUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface UserRepository extends JpaRepository<@Valid User, UUID> {
  boolean existsByEmail(String email);
  Optional<User> findByEmail(String email);

}
