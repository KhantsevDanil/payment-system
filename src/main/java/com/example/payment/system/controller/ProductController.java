package com.example.payment.system.controller;

import com.example.payment.system.entity.Product;
import com.example.payment.system.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    @PostMapping()
    public Product createStudent(@Valid @RequestBody Product std){
        return productService.save(std);
    }
    @GetMapping("/filtering")
    public List<Product> getEntities(@RequestParam(required = false) String name,
                                     @RequestParam(required = false) Double price,
                                     @RequestParam(required = false) UUID user) {
        return productService.findByFilters(name, price, user);
    }
}
